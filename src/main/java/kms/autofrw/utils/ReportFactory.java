package kms.autofrw.utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Author: minhquach
 * Date: 4/26/2019
 * Time: 11:48 AM
 */
public class ReportFactory {
    private static ExtentReports extent;
    private static ExtentTest reportLogger;
    private static String reportFilePath; //With slash ending
    private static String reportFolder;

    public ReportFactory(String reportFolder) {
        setReportFolder(reportFolder);
        initiateExtentReportInstance();
    }

    /**
     * Get the current report logger (ExtentTest) instance
     *
     * @return
     */
    public static ExtentTest getReportLogger() {
        return reportLogger;
    }

    /**
     * Get the current ExtentReports instance
     *
     * @return
     */
    private static ExtentReports getExtentReportInstance() {
        if (extent == null) {
            initiateExtentReportInstance();
        }
        return extent;
    }

    /**
     * Create a new ExtentTest with name and description
     *
     * @param testCaseName
     * @param testDescription
     * @return
     */
    public static ExtentTest newTestCase(String testCaseName, String testDescription) {
        reportLogger = getExtentReportInstance().createTest(testCaseName, testDescription);
        return reportLogger;
    }

    /**
     * Initialize the ExtentReports instance
     */
    private static void initiateExtentReportInstance() {
        reportFilePath = reportFolder + "TestReport_" + getCurrentDateTime() + ".html";
        ExtentHtmlReporter htmlReporter;
        htmlReporter = new ExtentHtmlReporter(reportFilePath);
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        htmlReporter.config().setDocumentTitle("Extent Report for QAADP FRW");
        htmlReporter.config().setReportName("Test report");
        htmlReporter.config().setTheme(Theme.DARK);
    }

    /**
     * Take screenshot of the browser
     *
     * @param driver
     * @return
     */
    public static String getScreenshot(WebDriver driver) {
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        String filename = "Screenshot_" + getCurrentDateTime() + ".png";
        String destination = reportFolder + filename;
        File finalDestination = new File(destination);
        try {
            FileUtils.copyFile(source, finalDestination);
        } catch (IOException e) {
            reportLogger.error("Could not take screen shot");
        }
        return filename;
    }

    /**
     * Get current date tim in "yyyy-MM-dd_hhmmss" format
     *
     * @return
     */
    private static String getCurrentDateTime() {
        return new SimpleDateFormat("yyyy-MM-dd_hhmmss").format(new Date());
    }

    /**
     * Set private reportFolder value
     *
     * @param path
     */
    private static void setReportFolder(String path) {
        reportFolder = path;
    }

    /**
     * Get the path to report file
     *
     * @return
     */
    public String getReportFilePath() {
        return reportFilePath;
    }

    /**
     * Write the report in temp to file
     */
    public void writeTestReport() {
        try {
            extent.flush();
        } catch (NullPointerException e) {
            Reporter.log(e.toString());
        }
    }

    /**
     * Get ExtentManager report instance location and open in default web browser
     */
    public void showReport(String filePath) {
        File htmlFile = new File(filePath);
        try {
            Desktop.getDesktop().browse(htmlFile.toURI());
        } catch (IOException e) {
            Reporter.log(e.toString());
        }
    }

    /**
     * Add details for the Environment section in report
     *
     * @param k
     * @param v
     */
    public void setEnvironmentInfo(String k, String v) {
        extent.setSystemInfo(k, v);
    }
}
