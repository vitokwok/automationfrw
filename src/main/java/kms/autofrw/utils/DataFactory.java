package kms.autofrw.utils;

import com.aventstack.extentreports.ExtentTest;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class DataFactory {
    private static ExtentTest reportLogger = ReportFactory.getReportLogger();

    private DataFactory() {
    }

    /**
     * Read the csv file and prepare data for test cases
     *
     * @param csvFilePath path to csv file
     * @param skipHeader  TRUE to skip header
     * @return
     */
    public static Object[][] readCSV(String csvFilePath, boolean skipHeader) {
        List<String[]> returnData = null;
        try (Reader reader = Files.newBufferedReader(Paths.get(csvFilePath))) {
            CSVReader csvReader;
            if (skipHeader) {
                csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();
            } else {
                csvReader = new CSVReader(reader);
            }
            returnData = csvReader.readAll();
        } catch (IOException e) {
            reportLogger.error(e);
        }
        //Convert to Object[][]
        if (returnData != null) {
            Object[][] returnObj = new Object[returnData.size()][];
            for (int i = 0; i < returnData.size(); i++) {
                returnObj[i] = returnData.get(i);
            }
            return returnObj;
        } else {
            //Return null object array if CSV file has no data
            return new Object[0][0];
        }
    }

}
