package kms.autofrw.pages;

import kms.autofrw.base.BasePage;

/**
 * Author: minhquach
 * Date: 4/22/2019
 * Time: 4:17 PM
 */
public class AccountPage extends BasePage<AccountPage> {

    public static final String PAGE_URL = "http://automationpractice.com/index.php?controller=my-account";

}
