package kms.autofrw.pages;

import kms.autofrw.base.BasePage;
import org.openqa.selenium.By;

/**
 * Author: minhquach
 * Date: 4/22/2019
 * Time: 4:17 PM
 */
public class SignInPage extends BasePage<SignInPage> {

    public static final String PAGE_URL = "http://automationpractice.com/index.php?controller=authentication&back=my-account";

    //user qaadp@mailinator.com / Test@123

    //Controls
    private By emailTxt = By.id("email");
    private By passwordTxt = By.id("passwd");
    private By signInBtn = By.id("SubmitLogin");

    /**
     * Input email and password then click Sign In button.
     *
     * @param email
     * @param password
     * @return Destination page is Account Page if login credential is correct
     */
    public AccountPage signIn(String email, String password) {
        findElement(emailTxt).sendKeys(email);
        findElement(passwordTxt).sendKeys(password);
        findElement(signInBtn).click();
        return new AccountPage();
    }

}
