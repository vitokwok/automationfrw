package kms.autofrw.pages;

import kms.autofrw.base.BasePage;
import org.openqa.selenium.By;

/**
 * Author: minhquach
 * Date: 4/22/2019
 * Time: 4:17 PM
 */
public class HomePage extends BasePage<HomePage> {

    protected static final String PAGE_URL = "http://automationpractice.com";

    //Buttons
    private By signInBtn = By.className("login");
    private By signOutBtn = By.className("logout");

    /**
     * Click the Sign In button to navigate to Sign In page
     *
     * @return
     */
    public SignInPage clickSignIn() {
        navigateToPage(signInBtn);
        return new SignInPage();
    }

    /**
     * Click the Sign Out button and navigate to Sign In page
     *
     * @return
     */
    public SignInPage signOut() {
        super.navigateToPage(signOutBtn);
        return new SignInPage();
    }

    /**
     * Launch Home Page
     *
     * @return
     */
    public HomePage launchHomePage() {
        return navigateToURL(PAGE_URL);
    }

}
