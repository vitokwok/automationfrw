/*
 * Copyright (c) 2019 Thermo Fisher Scientific
 * All rights reserved.
 */


package kms.autofrw.base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;


/**
 * Class use for initialize web driver
 */
public class DriverFactory {

    protected static WebDriver driver;
    private static String browser = System.getProperty("browserType");
    private static boolean isHeadless = Boolean.parseBoolean(System.getProperty("isHeadless"));

    private DriverFactory() {
    }

    /**
     * Initialize WebDriver
     */
    private static void initWebDriver() {
        switch (browser.toLowerCase()) {
            default:
            case "chrome":
                WebDriverManager.chromedriver().setup();
                ChromeOptions co = new ChromeOptions();
                co.addArguments("--disable-infobars");
                co.addArguments("--start-maximized");
                if (isHeadless)
                    co.addArguments("--headless");
                driver = new ChromeDriver(co);
                break;

            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                FirefoxOptions fo = new FirefoxOptions();
                fo.addArguments("--start-maximized");
                if (isHeadless)
                    fo.addArguments("--headless");
                driver = new FirefoxDriver(fo);
                break;

            case "internetexplorer":
                WebDriverManager.iedriver().setup();
                driver = new InternetExplorerDriver();
                break;

            case "edge":
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver();
                break;
        }
    }

    /**
     * Return the running driver instance. If not exist, initialize new one
     *
     * @return Return the web driver
     */
    public static WebDriver getDriver() {
        if (driver == null) {
            initWebDriver();
        }
        return driver;
    }

}

