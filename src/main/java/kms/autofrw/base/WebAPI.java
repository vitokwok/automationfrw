package kms.autofrw.base;

import com.aventstack.extentreports.ExtentTest;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import kms.autofrw.utils.ReportFactory;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Set;

/**
 * Author: minhquach
 * Date: 5/1/2019
 * Time: 11:46 AM
 */
public class WebAPI {
    protected ExtentTest reportLogger = ReportFactory.getReportLogger();
    protected HttpResponse response = null;
    private String strHeaders = "headers";
    private String strBody = "body";

    /**
     * Read request details json file and prepare then send request to server
     *
     * @param host     server host
     * @param fileName request details json file path
     * @return return the response after sending request
     */
    private HttpResponse prepareAndSendRequest(String host, String fileName) {

        HttpRequestBase request = new HttpRequestBase() {
            @Override
            public String getMethod() {
                return null;
            }
        };

        JsonObject jsonObject = readJsonFile(fileName).getAsJsonObject("request");
        String requestMethod = jsonObject.get("Method").getAsString();
        String requestURI = host + jsonObject.get("URI").getAsString();

        //Set request method and body if is POST/PUT
        switch (requestMethod.toUpperCase()) {
            case "GET":
                request = new HttpGet(requestURI);
                break;
            case "POST":
                request = setRequestBody(jsonObject, new HttpPost(requestURI));
                break;
            case "PUT":
                request = setRequestBody(jsonObject, new HttpPut(requestURI));
                break;
            default:
                //TODO: error if could not get request method
                break;
        }

        //Set request header
        request.setHeaders(getHeadersFromJsonObject(jsonObject));

        //Send the request and get the response
        try {
            return HttpClientBuilder.create().build().execute(request);

        } catch (IOException e) {
            reportLogger.error("Request sending error");
        }
        return null;
    }

    /**
     * Read request details json file and prepare then send request to server. This call the private method prepareAndSendRequest(host, fileName)
     *
     * @param host     server host
     * @param fileName request details json file path
     */
    public void sendRequest(String host, String fileName) {
        response = prepareAndSendRequest(host, fileName);
    }

    /**
     * Read the input JsonObject and set it as the request body
     *
     * @param jsonObject JsonObject to convert and set as request body
     * @param request    target request to be added with body
     * @return the request with body set
     */
    private HttpRequestBase setRequestBody(JsonObject jsonObject, HttpEntityEnclosingRequestBase request) {
        String jo = jsonObject.getAsJsonObject(strBody).toString();
        try {
            StringEntity se = new StringEntity(jo);
            request.setEntity(se);
        } catch (UnsupportedEncodingException e) {
            reportLogger.error("Could not set body");
        }
        return request;
    }

    /**
     * Convert headers in JsonObject into Headers[]
     *
     * @param jsonObject JsonObject contains the Headers details
     * @return converted headers array
     */
    private Header[] getHeadersFromJsonObject(JsonObject jsonObject) {
        //If headers part in json file is null, return an empty array
        if (!jsonObject.get(strHeaders).isJsonNull()) {
            JsonObject jsonObjectRequest = jsonObject.getAsJsonObject(strHeaders);
            Set<Map.Entry<String, JsonElement>> entries = jsonObjectRequest.entrySet();
            Header[] headers = new Header[entries.size()];
            int i = 0;
            for (Map.Entry<String, JsonElement> entry : entries) {
                BasicHeader header = new BasicHeader(entry.getKey(), entry.getValue().getAsString());
                headers[i++] = header;
            }
            return headers;
        } else {
            return new Header[]{};
        }
    }

    /**
     * Read the out json file
     *
     * @param filePath path to the file
     * @return Return a JsonObject if read file properly, else return null
     */
    private JsonObject readJsonFile(String filePath) {
        JsonParser jsonParser = new JsonParser();
        String fileContent;
        try {
            fileContent = new String(Files.readAllBytes(Paths.get(filePath)));
            return jsonParser.parse(fileContent).getAsJsonObject();
        } catch (NoSuchFileException e) {
            reportLogger.error("Request definition json file " + filePath + " not found");
            reportLogger.error(e);
            Assert.fail("Request definition json file not found");
            throw new NullPointerException();
        } catch (IOException e) {
            reportLogger.error("Read request definition json file " + filePath + " error");
            reportLogger.error(e);
            Assert.fail("Read request definition json file error");
            throw new NullPointerException();
        }
    }

    public void verifyResponse(String fileName) {
        verifyResponseDetails(fileName);
    }

    /**
     * Verify response code, headers, body if expected in request details json file
     *
     * @param fileName request details json file
     */
    private void verifyResponseDetails(String fileName) {
        //TODO: verify ignore dynamic value
        JsonObject inputJsonObject = readJsonFile(fileName).getAsJsonObject("response");

        //Verify response code if expected
        verifyStatusCode(inputJsonObject);

        //Verify response headers if expected
        verifyHeaders(inputJsonObject);

        //Verify response body if expected
        verifyBody(inputJsonObject);
    }

    private void verifyBody(JsonObject inputJsonObject) {
        if (!inputJsonObject.get(strBody).isJsonNull()) {
            try {
                boolean bodyVerify = false;
                String strActualBody = "", strExpectedBody = "";
                String content = EntityUtils.toString(response.getEntity()); //Catch exception here
                JsonElement actualJsonElement = new JsonParser().parse(content);
                //Handle the JsonElement
                if (actualJsonElement.isJsonObject()) {
                    JsonObject actualJsonObject = actualJsonElement.getAsJsonObject();
                    JsonObject expectedJsonObject = inputJsonObject.getAsJsonObject(strBody);
                    bodyVerify = actualJsonObject.equals(expectedJsonObject);
                    strActualBody = actualJsonObject.toString();
                    strExpectedBody = expectedJsonObject.toString();
                }

                if (actualJsonElement.isJsonArray()) {
                    JsonArray expectedJsonArray = inputJsonObject.getAsJsonArray(strBody);
                    bodyVerify = actualJsonElement.equals(expectedJsonArray);
                    strActualBody = actualJsonElement.toString();
                    strExpectedBody = expectedJsonArray.toString();
                }

                if (bodyVerify) {
                    reportLogger.createNode("Response body is as expected");
                } else {
                    reportLogger.createNode("Response body is not as expected").fail(strActualBody).fail(strExpectedBody);
                    Assert.fail(strActualBody);
                }

            } catch (IOException e) {
                reportLogger.error("Body part is missing");
            }
        }

    }

    //
    private void verifyHeaders(JsonObject inputJsonObject) {
        if (!inputJsonObject.get(strHeaders).isJsonNull()) {
            JsonObject expectedJsonObject = inputJsonObject.getAsJsonObject(strHeaders);
            JsonObject actualJsonObject = new JsonObject();
            Header[] actualHeaders = response.getAllHeaders();

            for (Header header : actualHeaders) {
                String headerValue = "\"" + header.getValue() + "\"";
                JsonElement je = new JsonParser().parse(headerValue);
                actualJsonObject.add(header.getName(), je);
            }
            if (actualJsonObject.equals(expectedJsonObject)) {
                reportLogger.createNode("Response headers are as expected");
            } else {
                reportLogger.createNode("Response headers are not as expected").fail(actualJsonObject.toString()).fail(expectedJsonObject.toString());
//                reportLogger.createNode("Response headers are not as expected").fail(actualJsonObject.getAsString());
            }
            Assert.assertEquals(actualJsonObject, expectedJsonObject);
        }

    }

    private void verifyStatusCode(JsonObject inputJsonObject) {
        if (!inputJsonObject.get("code").isJsonNull()) {
            int expectedCode = inputJsonObject.get("code").getAsInt();
            int actualCode = response.getStatusLine().getStatusCode();
            if (expectedCode == actualCode) {
                reportLogger.createNode("Response code is as expected: " + expectedCode);
            } else {
                reportLogger.createNode("Response code is incorrect:").fail("Actual: " + actualCode).fail("Expected: " + expectedCode);
            }
            Assert.assertEquals(actualCode, expectedCode);
        }

    }

}
