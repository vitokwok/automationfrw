package kms.autofrw.base;

import com.aventstack.extentreports.ExtentTest;
import kms.autofrw.utils.ReportFactory;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Author: minhquach
 * Date: 4/20/2019
 * Time: 7:24 AM
 */
public class BaseTest {

    protected static ExtentTest reportLogger;
    private static String browserType = "chrome";
    private static boolean isHeadless = false;
    private static ReportFactory report;
    private static String reportFolder = "reports/"; //With slash ending
    //WebDriver properties
    private static WebDriver driver;
    //ExtentManager properties
    private boolean openReportAfterExecuted = true;

    @BeforeMethod(alwaysRun = true)
    public static void beforeMethod(Method method) {
        //Get test method testName and description properties and set to ExtentManager test instance
        Test test = method.getAnnotation(Test.class);
        if (test == null) {
            return;
        }
        reportLogger = ReportFactory.newTestCase(test.testName(), test.description());
    }

    @BeforeGroups(groups = {"WebUI"}, alwaysRun = false)
    protected static void setUpWebUI() {
        report = new ReportFactory(reportFolder);
        //Only shows WARNING of org.open.selenium class
        Logger.getLogger("org.openqa.selenium").setLevel(Level.WARNING);
        System.setProperty("browserType", browserType);
        System.setProperty("isHeadless", Boolean.toString(isHeadless));
        driver = DriverFactory.getDriver();
        //Log browser details for web testing
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browserVersion = cap.getBrowserName() + " " + cap.getVersion();
        report.setEnvironmentInfo("Browser", browserVersion);
        //Log OS details
        report.setEnvironmentInfo("OS", System.getProperty("os.name"));
    }

    @BeforeGroups(groups = {"WebAPI"})
    protected static void setUpWebAPI() {
        report = new ReportFactory(reportFolder);
    }

    @AfterGroups(groups = {"WebUI"})
    protected void endGroupWebUI() {
        try {
            driver.quit();
        } catch (Exception e) {
            reportLogger.createNode("Error");
            reportLogger.error(e);
        }
    }


    @AfterSuite(alwaysRun = true)
    protected void endSuite() {
        //Write out the report
        report.writeTestReport();
        //Auto open report in default browser if set
        if (openReportAfterExecuted)
            report.showReport(report.getReportFilePath());
    }

}
