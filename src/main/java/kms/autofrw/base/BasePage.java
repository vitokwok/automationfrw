package kms.autofrw.base;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import kms.autofrw.utils.ReportFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.IOException;

/**
 * Author: minhquach
 * Date: 4/22/2019
 * Time: 3:51 PM
 */
public class BasePage<P> {
    private static final int TIMEOUT = 10;
    private static final int POLLING = 100;
    protected WebDriver driver = DriverFactory.getDriver();
    protected ExtentTest reportLogger = ReportFactory.getReportLogger();
    private WebDriverWait wait;
    P destinationPage;

    /**
     * Define default implicit wait time
     */
    public BasePage() {
        wait = new WebDriverWait(driver, TIMEOUT, POLLING);
    }

    //Public methods to use in test

    /**
     * Verify if a message exist on page
     *
     * @param messageContent Expected message content
     */
    public void verifyMessageExists(String messageContent) {
        By by = By.xpath("//*[contains(text(),'" + messageContent + "')]");
        if (!driver.findElements(by).isEmpty()) {
            String msg = "Message \"" + messageContent + "\" exists on page";
            reportLogger.createNode(msg);
            Assert.assertEquals(messageContent, messageContent);
        } else {
            try {
                String msg = "Message \"" + messageContent + "\" does not exist on page";
                String screenShotPath = ReportFactory.getScreenshot(driver);
                reportLogger.createNode(msg).fail("Screenshot",MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build());
                Assert.assertEquals(msg, messageContent);
            } catch (IOException e) {
                reportLogger.error("Could not attach screen shot");
            }

        }
    }

    //Protected methods to shared with PO

    /**
     * Navigate to an URL
     *
     * @param url URL to navigate to
     * @return destination page object
     */
    protected P navigateToURL(String url) {
        driver.get(url);
        return destinationPage;
    }

    /**
     * Navigate to a page by click on an element
     *
     * @param by By locator of element to be clicked on
     */
    protected void navigateToPage(By by) {
        waitForElement(by);
        findElement(by).click();
    }

    /**
     * Return WebElement for a By locator
     *
     * @param by By locator
     * @return WebElement
     */
    protected WebElement findElement(By by) {
        return driver.findElement(by);
    }

    /**
     * Wait for element to be clickable
     *
     * @param by By locator
     */
    private void waitForElement(By by) {
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    /**
     * Verify the current page URL is as expected
     *
     * @param url Expected URL
     */
    public void verifyPageLocation(String url) {
        String currentURL = driver.getCurrentUrl();
        if (currentURL.equals(url)) {
            reportLogger.createNode("Destination page URL is correctly: \"" + url + "\"");
        } else {
            reportLogger.createNode("Current URL is not correct").fail("Current page URL is: \"" + currentURL + "\nnot: \"" + url + "\"");
        }
        Assert.assertEquals(url, url);
    }

}
