package WebAPITests;

import kms.autofrw.base.BaseTest;
import kms.autofrw.base.WebAPI;
import kms.autofrw.utils.DataFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Author: minhquach
 * Date: 5/1/2019
 * Time: 11:48 AM
 */
public class WebAPITests extends BaseTest {
    //Data
    private String csvFilePath = "data/endpoints.csv";
    private String host = "http://dummy.restapiexample.com";

    @DataProvider(name = "EndPoints")
    private Object[][] badCredentials() {
        return DataFactory.readCSV(csvFilePath, true);
    }

    @Test(testName = "API_TestCase", dataProvider = "EndPoints", groups = {"WebAPI"})
    public void WebApiTests(String requestName, String jsonFileName) {
        reportLogger.info("Test request " + requestName);
        WebAPI ws = new WebAPI();
        ws.sendRequest(host, jsonFileName);
        ws.verifyResponse(jsonFileName);
    }

}
