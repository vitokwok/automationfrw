package WebUITests;

import kms.autofrw.utils.DataFactory;
import kms.autofrw.base.BaseTest;
import kms.autofrw.pages.AccountPage;
import kms.autofrw.pages.HomePage;
import kms.autofrw.pages.SignInPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//@Listeners(ExtentITestListenerClassAdapter.class)
public class WebUITests extends BaseTest {

    //Data
    private String csvFilePath = "data/credentials.csv";

    @DataProvider(name = "BadCredentials", indices = {1, 2, 3})
    private Object[][] badCredentials() {
        return DataFactory.readCSV(csvFilePath, true);
    }

    @DataProvider(name = "ValidCredential", indices = {0})
    private Object[][] validCredentials() {
        return DataFactory.readCSV(csvFilePath, true);
    }

    @Test(priority = 1, testName = "TC1", description = "Test login function with bad credentials", dataProvider = "BadCredentials", groups = {"WebUI"})
    public void TestCase1(String username, String password, String message) {
        reportLogger.info("Launch homepage");
        HomePage homepage = new HomePage();
        homepage.launchHomePage();
        reportLogger.info("Click signIn link");
        SignInPage signinPage = homepage.clickSignIn();
        reportLogger.info("Login to system with " + username + "/" + password);
        signinPage.signIn(username, username);
        signinPage.verifyMessageExists(message);
    }

    @Test(priority = 1, testName = "TC2", description = "Test login valid credential", dataProvider = "ValidCredential", groups = {"WebUI"})
    public void TestCase2(String username, String password) {
        reportLogger.info("Launch homepage");
        HomePage homepage = new HomePage();
        homepage.launchHomePage();
        reportLogger.info("Click signIn link");
        SignInPage signinPage = homepage.clickSignIn();
        reportLogger.info("Login to system with " + username + "/" + password);
        AccountPage accountPage = signinPage.signIn(username, password);
        accountPage.verifyPageLocation(AccountPage.PAGE_URL);
        reportLogger.info("Logout system");
        homepage.signOut();
        signinPage.verifyPageLocation(SignInPage.PAGE_URL);
    }
}

